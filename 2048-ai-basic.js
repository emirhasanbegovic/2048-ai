function startBasicAI() {
	function basicAI() {
		if (grid.moveLeft() || grid.moveUp() || grid.moveRight() || grid.moveDown()) {
			grid.spawnRandomNumber();
			setTimeout(basicAI, 30);
		} else {
			grid.isGameOver();
			alert("GameOver");
		}
		draw();
	}
	basicAI();
}
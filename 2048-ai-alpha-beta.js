function startAlfaBetaAI() {
	var startDepth = 8;
	var weightMatrix = [
		[8, 5, 4, 3],
		[5, 4, 3, 2],
		[4, 3, 2, 1],
		[3, 2, 1, 0]
	];
	var directionAlphaBetaMinMax = "";

	function alfaBetaAI() {
		var computeNextMove = function() {
			var dir = alphaBetaMinMax(startDepth, grid, true, Number.NEGATIVE_INFINITY, Number.POSITIVE_INFINITY, 0, 0);
			switch (directionAlphaBetaMinMax) {
				case "up":
					grid.moveUp();
					break;
				case "down":
					grid.moveDown();
					break;
				case "left":
					grid.moveLeft();
					break;
				case "right":
					grid.moveRight();
					break;
			}
			grid.isGameOver();
			if (!grid.gameOver) {
				grid.spawnRandomNumber();
				draw();
				setTimeout(computeNextMove, 1);
			} else {
				alert("GameOver");
			}
		}
		computeNextMove();
	}

	function weightHeuristic(grid) {
		var sum = 0;
		for (var i = 0; i < 4; i++) {
			for (var j = 0; j < 4; j++) {
				if (grid.grid[i][j] !== null) {
					sum += weightMatrix[i][j] * grid.grid[i][j].value;
				}
			}
		}
		return sum;
	}

	function penaltyHeuristic(grid) {
		var penalty = 0;
		for (var i = 0; i < 4; i++) {
			for (var j = 0; j < 4; j++) {
				if (grid.grid[i][j] !== null) {
					penalty += neighbourPenalty(i, j, grid, grid.grid[i][j].value);
				}
			}
		}
		return penalty;
	}

	function neighbourPenalty(i, j, grid, value) {
		var penalty = 0;
		if (j + 1 <= 3) {
			if (grid.grid[i][j + 1] !== null) {
				penalty += Math.abs(value - grid.grid[i][j + 1].value);
			}
		}
		if (i + 1 <= 3) {
			if (grid.grid[i + 1][j] !== null) {
				penalty += Math.abs(value - grid.grid[i + 1][j].value);
			}
		}

		if (j - 1 >= 0) {
			if (grid.grid[i][j - 1] !== null) {
				penalty += Math.abs(value - grid.grid[i][j - 1].value);
			}
		}
		if (i - 1 >= 0) {
			if (grid.grid[i - 1][j] !== null) {
				penalty += Math.abs(value - grid.grid[i - 1][j].value);
			}
		}
		return penalty;
	}

	function alphaBetaMinMax(depth, newGrid, player, alpha, beta, st2, st4) {
		if (depth == 0) {
			newGrid.isGameOver();
			var wh = weightHeuristic(newGrid);
			var ph = penaltyHeuristic(newGrid);

			if (newGrid.gameOver) {
				return -10000000 * Math.pow(0.9, st2) * Math.pow(0.1, st4);
			}

			return (wh - ph) * Math.pow(0.9, st2) * Math.pow(0.1, st4);
		}


		if (player) {

			var tempGrid = newGrid.copy();
			if (tempGrid.moveUp()) {
				var recursionVal = alphaBetaMinMax(depth - 1, tempGrid, !player, alpha, beta, st2, st4);

				if (recursionVal > alpha) {
					alpha = recursionVal;

					if (startDepth == depth) {
						directionAlphaBetaMinMax = "up";
					}
				}

				if (alpha >= beta)
					return alpha;

				tempGrid = newGrid.copy();
			}

			if (tempGrid.moveDown()) {

				var recursionVal = alphaBetaMinMax(depth - 1, tempGrid, !player, alpha, beta, st2, st4);
				if (recursionVal > alpha) {
					alpha = recursionVal;

					if (startDepth == depth) {

						directionAlphaBetaMinMax = "down";
					}

				}

				if (alpha >= beta)
					return alpha;

				tempGrid = newGrid.copy();
			}

			if (tempGrid.moveLeft()) {
				var recursionVal = alphaBetaMinMax(depth - 1, tempGrid, !player, alpha, beta, st2, st4);

				if (recursionVal > alpha) {
					alpha = recursionVal;
					if (startDepth == depth) {

						directionAlphaBetaMinMax = "left";
					}

				}

				if (alpha >= beta)
					return alpha;

				tempGrid = newGrid.copy();
			}


			if (tempGrid.moveRight()) {

				var recursionVal = alphaBetaMinMax(depth - 1, tempGrid, !player, alpha, beta, st2, st4);
				if (recursionVal > alpha) {
					alpha = recursionVal;
					if (startDepth == depth) {

						directionAlphaBetaMinMax = "right";
					}

				}
			}
			if (alpha == Number.NEGATIVE_INFINITY) {
				return -10000000 * Math.pow(0.9, st2) * Math.pow(0.1, st4);;
			}
			return alpha;
		}

		var tempGrid = newGrid.copy();

		var emptySpots = tempGrid.getEmptySpots();

		for (var i = 0; i < emptySpots.length; i++) {
			var s = emptySpots[i];
			tempGrid.grid[s.i][s.j] = new Tile(2);

			var r = alphaBetaMinMax(depth - 1, tempGrid, !player, alpha, beta, st2 + 1, st4);

			var recursionVal = r;


			if (recursionVal < beta) {

				beta = recursionVal;
			}
			if (alpha >= beta)
				break;


			tempGrid.grid[s.i][s.j] = new Tile(4);
			var r1 = alphaBetaMinMax(depth - 1, tempGrid, !player, alpha, beta, st2, st4 + 1);

			var recursionVal1 = r1;

			if (recursionVal1 < beta) {

				beta = recursionVal1;
			}
			if (alpha >= beta)
				break;

			tempGrid.grid[s.i][s.j] = null;
		}

		if (beta == Number.POSITIVE_INFINITY) {
			return 10000000 * Math.pow(0.9, st2) * Math.pow(0.1, st4);;
		}
		return beta;
	}

	alfaBetaAI();
}
/*
 * @authors: Andrej Martinovič, Emir Hasanbegovič
 */
 
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>

typedef struct {
    int grid[4][4];
    bool merges[4][4];
}Grid;

void initGrid(Grid*);
void printGrid(Grid*);
void spawnRandomNumber(Grid*);
int* emptySpots(Grid*);
bool moveUp(Grid*);
int calcPositionUp(int, int, Grid*, int);
bool moveDown(Grid*);
int calcPositionDown(int, int, Grid*, int);
bool moveRight(Grid*);
int calcPositionRight(int, int, Grid*, int);
bool moveLeft(Grid*);
int calcPositionLeft(int, int, Grid*, int);
bool hasNeighbour(int,int,int,Grid*);
bool isGameOver(Grid*);
Grid* copyGrid(Grid*);
void runExpectiMaxAI(Grid*);
int weightHeuristic(Grid*);
double expectiMaxAI(int, Grid*, bool);

int main()
{
    srand(time(NULL));
    Grid grid;
    initGrid(&grid);
    printGrid(&grid);
    
    runExpectiMaxAI(&grid);
    
    /*
    while(!isGameOver(&grid)){
        char key = '1';
        scanf("%c",&key);
        if(key == 'w'){
            if(moveUp(&grid)){
                spawnRandomNumber(&grid);
            }
            printGrid(&grid);
        }else if(key == 's'){
            if(moveDown(&grid)){
                spawnRandomNumber(&grid);
            }
             printGrid(&grid);
        }else if(key == 'd'){
            if(moveRight(&grid)){
                spawnRandomNumber(&grid);
            }
            printGrid(&grid);
        }else if(key == 'a'){
            if(moveLeft(&grid)){
                spawnRandomNumber(&grid);
            }
            printGrid(&grid);
        }
    }*/
    
    printf("GAMEOVER\n");
    return 0;
}

void printGrid(Grid* grid){
    printf("\nGRID\n");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            printf("%d      ",grid->grid[i][j]);
        }
        printf("\n");
    }
    /*printf("BOOL\n");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            printf("%d ",grid->merges[i][j]);
        }
        printf("\n");
    }*/
}

void initGrid(Grid* grid){
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4; j++){
            grid->grid[i][j] = 0;
            grid->merges[i][j] = false;
        }
    }
    spawnRandomNumber(grid);
    spawnRandomNumber(grid);
}

void spawnRandomNumber(Grid* grid){
    int *e;
    e = emptySpots(grid);
    if(e[16] == 0)
        return;

    int tile = (rand()%100 <= 90)?2:4;
    int tileIndex = e[rand()%e[16]];
    int y = tileIndex / 4;
    int x = tileIndex % 4;
    
    grid->grid[y][x] = tile;
    free(e);
}

int* emptySpots(Grid* grid){
    int *e = (int*)calloc(17, sizeof(int));
    e[16] = 0;
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4; j++){
            if(grid->grid[i][j] == 0){
                int val = 4*i+j;
                e[e[16]] = val;
                e[16]++;
            }
        }
    }
    return e;
}

bool moveUp(Grid* grid){
    bool changed = false;
    int *mergedArray = (int*)calloc(17,sizeof(int));
    
    for(int i = 1; i < 4; i++){
        for(int j = 0; j < 4; j++){
            if(grid->grid[i][j] != 0){
                int ret = calcPositionUp(i,j,grid,grid->grid[i][j]);
                if(ret != -100){
                    changed = true;
                    if(ret != -10){
                        mergedArray[mergedArray[16]] = ret;
                        mergedArray[16]++;
                    }
                }
                
            }
        }
    }
    
    if(changed){
        for(int i = 0; i < mergedArray[16]; i++){
            int y = mergedArray[i] / 4;
            int x = mergedArray[i] % 4;
            grid->merges[y][x] = false;
        }    
    }
    free(mergedArray);
    return changed;
}

int calcPositionUp(int y, int x, Grid* grid, int number){
    // ni spremembe -100
    // je move = -10
    // je merge bo cifra 4*y+x
    int boardChanged = -100;
    int oldY = y;
    while(y--){
        if(grid->grid[y][x] == 0){
            grid->grid[y][x] = number;
            grid->grid[oldY][x] = 0;
            oldY = y;
            boardChanged = -10;
        }else{
            if(grid->grid[y][x] == number && !grid->merges[y][x]){
                grid->grid[y][x] += number;
                grid->merges[y][x] = true;
                grid->grid[oldY][x] = 0;
                return 4*y+x;
            }
            
            if(grid->grid[y][x] != 0 && grid->grid[y][x] != number){
                break;
            }
        }
    }
    return boardChanged;
}

bool moveDown(Grid* grid){
    bool changed = false;
    int *mergedArray = (int*)calloc(17,sizeof(int));
    
    for(int i = 2; i >= 0; i--){
        for(int j = 0; j < 4; j++){
            if(grid->grid[i][j] != 0){
                int ret = calcPositionDown(i,j,grid,grid->grid[i][j]);
                if(ret != -100){
                    changed = true;
                    if(ret != -10){
                        mergedArray[mergedArray[16]] = ret;
                        mergedArray[16]++;
                    }
                }
                
            }
        }
    }
    
    if(changed){
        for(int i = 0; i < mergedArray[16]; i++){
            int y = mergedArray[i] / 4;
            int x = mergedArray[i] % 4;
            grid->merges[y][x] = false;
        }    
    }
    free(mergedArray);
    return changed;
}

int calcPositionDown(int y, int x, Grid* grid, int number){
    // ni spremembe -100
    // je move = -10
    // je merge bo cifra 4*y+x
    int boardChanged = -100;
    int oldY = y;
    while(y < 3){
        y++;
        if(grid->grid[y][x] == 0){
            grid->grid[y][x] = number;
            grid->grid[oldY][x] = 0;
            oldY = y;
            boardChanged = -10;
        }else{
            if(grid->grid[y][x] == number && !grid->merges[y][x]){
                grid->grid[y][x] += number;
                grid->merges[y][x] = true;
                grid->grid[oldY][x] = 0;
                return 4*y+x;
            }
            
            if(grid->grid[y][x] != 0 && grid->grid[y][x] != number){
                break;
            }
        }
    }
    return boardChanged;
}

bool moveRight(Grid* grid){
    bool changed = false;
    int *mergedArray = (int*)calloc(17,sizeof(int));
    
    for(int i = 0; i < 4; i++){
        for(int j = 2; j >= 0; j--){
            if(grid->grid[i][j] != 0){
                int ret = calcPositionRight(i,j,grid,grid->grid[i][j]);
                if(ret != -100){
                    changed = true;
                    if(ret != -10){
                        mergedArray[mergedArray[16]] = ret;
                        mergedArray[16]++;
                    }
                }
                
            }
        }
    }
    
    if(changed){
        for(int i = 0; i < mergedArray[16]; i++){
            int y = mergedArray[i] / 4;
            int x = mergedArray[i] % 4;
            grid->merges[y][x] = false;
        }    
    }
    free(mergedArray);
    return changed;
}

int calcPositionRight(int y, int x, Grid* grid, int number){
    // ni spremembe -100
    // je move = -10
    // je merge bo cifra 4*y+x
    int boardChanged = -100;
    int oldX = x;
    while(x < 3){
        x++;
        if(grid->grid[y][x] == 0){
            grid->grid[y][x] = number;
            grid->grid[y][oldX] = 0;
            oldX = x;
            boardChanged = -10;
        }else{
            if(grid->grid[y][x] == number && !grid->merges[y][x]){
                grid->grid[y][x] += number;
                grid->merges[y][x] = true;
                grid->grid[y][oldX] = 0;
                return 4*y+x;
            }
            
            if(grid->grid[y][x] != 0 && grid->grid[y][x] != number){
                break;
            }
        }
    }
    return boardChanged;
}

bool moveLeft(Grid* grid){
    bool changed = false;
    int *mergedArray = (int*)calloc(17,sizeof(int));
    
    for(int i = 0; i < 4; i++){
        for(int j = 1; j < 4; j++){
            if(grid->grid[i][j] != 0){
                int ret = calcPositionLeft(i,j,grid,grid->grid[i][j]);
                if(ret != -100){
                    changed = true;
                    if(ret != -10){
                        mergedArray[mergedArray[16]] = ret;
                        mergedArray[16]++;
                    }
                }
                
            }
        }
    }
    
    if(changed){
        for(int i = 0; i < mergedArray[16]; i++){
            int y = mergedArray[i] / 4;
            int x = mergedArray[i] % 4;
            grid->merges[y][x] = false;
        }    
    }
    free(mergedArray);
    return changed;
}

int calcPositionLeft(int y, int x, Grid* grid, int number){
    // ni spremembe -100
    // je move = -10
    // je merge bo cifra 4*y+x
    int boardChanged = -100;
    int oldX = x;
    while(x--){
        if(grid->grid[y][x] == 0){
            grid->grid[y][x] = number;
            grid->grid[y][oldX] = 0;
            oldX = x;
            boardChanged = -10;
        }else{
            if(grid->grid[y][x] == number && !grid->merges[y][x]){
                grid->grid[y][x] += number;
                grid->merges[y][x] = true;
                grid->grid[y][oldX] = 0;
                return 4*y+x;
            }
            
            if(grid->grid[y][x] != 0 && grid->grid[y][x] != number){
                break;
            }
        }
    }
    return boardChanged;
}

bool isGameOver(Grid *grid){
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4; j++){
            if(grid->grid[i][j] == 0 || hasNeighbour(i,j,grid->grid[i][j], grid)){
                return false;
            }
        }
    }
    return true;
}

bool hasNeighbour(int i,int j,int number,Grid *grid){
    if (j + 1 <= 3 && grid->grid[i][j + 1] != 0) {
		if (grid->grid[i][j + 1] == number) {
			return true;
		}
	}
	
	if (i + 1 <= 3 && grid->grid[i + 1][j] != 0) {
		if (grid->grid[i + 1][j] == number) {
			return true;
		}
	}

	return false;
}

Grid* copyGrid(Grid* grid){
    Grid *newGrid = (Grid*)malloc(sizeof(Grid));
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4;j++){
            newGrid->grid[i][j] = grid->grid[i][j];
            newGrid->merges[i][j] = false;
        }
    }
    
    return newGrid;
}

int direction = -1;
int startDepth = 5;
int weightMatrix[4][4];
                    
/* EXPECTIMAX AI */
void runExpectiMaxAI(Grid *grid){
    weightMatrix[0][0] = 8;
    weightMatrix[0][1] = 4;
    weightMatrix[0][2] = 2;
    weightMatrix[0][3] = 1;
    weightMatrix[1][0] = 16;
    weightMatrix[1][1] = 32;
    weightMatrix[1][2] = 64;
    weightMatrix[1][3] = 128;
    weightMatrix[2][0] = 2048;
    weightMatrix[2][1] = 1024;
    weightMatrix[2][2] = 512;
    weightMatrix[2][3] = 256;
    weightMatrix[3][0] = 4096;
    weightMatrix[3][1] = 8192;
    weightMatrix[3][2] = 16384;
    weightMatrix[3][3] = 32768;
    
    while(!isGameOver(grid)){
        expectiMaxAI(startDepth, grid, true);
        switch(direction){
            case 0:
                moveUp(grid);
                break;
            case 1:
                moveDown(grid);
                break;
            case 2:
                moveLeft(grid);
                break;
            case 3:
                moveRight(grid);
                break;
            default:
                printf("DEATH");
                printGrid(grid);
                return;
        }
        
        direction = -1;
        spawnRandomNumber(grid);
        //printGrid(grid);
    }
    printGrid(grid);
}

double expectiMaxAI(int depth, Grid *grid, bool player){
    if(depth == 0){
        if(isGameOver(grid)){
            return -100000000.0;
        }
        return (double) weightHeuristic(grid);
    }
    
    if(player){
        Grid *newGrid = copyGrid(grid);
        int maxCifra = INT_MIN;
        if(moveUp(newGrid)){
            int c = expectiMaxAI(depth - 1, newGrid, !player);

            if(maxCifra < c){
                maxCifra = c;
                if(depth == startDepth){
                    direction = 0;
                }
            }
            free(newGrid);
            newGrid = copyGrid(grid);
        }
        
       if(moveDown(newGrid)){
            int c = expectiMaxAI(depth - 1, newGrid, !player);
            
            if(maxCifra < c){
                maxCifra = c;
                if(depth == startDepth){
                    direction = 1;
                }
            }
            free(newGrid);
            newGrid = copyGrid(grid);
        }
        
        if(moveLeft(newGrid)){
            int c = expectiMaxAI(depth - 1, newGrid, !player);
            
            if(maxCifra < c){
                maxCifra = c;
                if(depth == startDepth){
                    direction = 2;
                }
            }
            free(newGrid);
            newGrid = copyGrid(grid);
        }
        
        if(moveRight(newGrid)){
            int c = expectiMaxAI(depth - 1, newGrid, !player);
            
            if(maxCifra < c){
                maxCifra = c;
                if(depth == startDepth){
                    direction = 3;
                }
            }
        }
        
        free(newGrid);
        
        if(maxCifra == INT_MIN){
            return -100000000.0;
        }
        return maxCifra;
    }

    Grid *newGrid = copyGrid(grid);

    int *e = emptySpots(newGrid);
    double avgScore = 0.0;
    
    for(int i = 0; i < e[16]; i++){
        int y = e[i] / 4;
        int x = e[i] % 4;

        newGrid->grid[y][x] = 2;
        avgScore += 0.9 * expectiMaxAI(depth - 1, newGrid, !player);
        
        newGrid->grid[y][x] = 4;
        avgScore += 0.1 * expectiMaxAI(depth - 1, newGrid, !player);
        newGrid->grid[y][x] = 0;
    }

    int value = e[16];
    free(e);
    free(newGrid);
    
    return avgScore / (2 * value);
}

int weightHeuristic(Grid *grid){
    int sum = 0;
    
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4; j++){
            if(grid->grid[i][j] != 0){
                sum += weightMatrix[i][j] * grid->grid[i][j];
            }
        }
    }
    return sum;
}

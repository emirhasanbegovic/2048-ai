function startExpectiMaxAI() {
	var direction = -1;
	var depth = 5;
	var startDepth = depth;
	var weightMatrix = [
		[8, 4, 2, 1],
		[16, 32, 64, 128],
		[2048, 1024, 512, 256],
		[4096, 8192, 16384, 32768]
	];
	var howMany = 30;

	function restartAI() {
		grid = new Grid();
		grid.initGrid();
		if (howMany > 0) {
			howMany--;
			startExpectiMaxAI();
		}
	}

	function startAI() {
		var computeNextMove = function() {
			var dir = expectiMax(depth, grid, true);

			switch (direction) {
				case 0:
					grid.moveUp();
					break;
				case 1:
					grid.moveDown();
					break;
				case 2:
					grid.moveLeft();
					break;
				case 3:
					grid.moveRight();
					break;
				default:
					var arr = [];
					for (var i = 0; i < 4; i++) {
						for (var j = 0; j < 4; j++) {
							arr.push(grid.grid[i][j].value);
						}
					}
					arr.sort(function(a, b) {
						return b - a;
					});
					restartAI();
					console.log(arr);
					return;
			}

			direction = -1;
			grid.spawnRandomNumber();
			draw();
			setTimeout(computeNextMove, 1);
		}
		computeNextMove();
	}

	function expectiMax(depth, newGrid, player) {
		if (depth == 0) {
			newGrid.isGameOver();
			if (newGrid.gameOver) {
				return -100000000;
			}
			return weightHeuristic(newGrid);
		}

		if (player) {
			var tempGrid = newGrid.copy();
			var maxCifra = Number.NEGATIVE_INFINITY;
			if (tempGrid.moveUp()) {
				var c = expectiMax(depth - 1, tempGrid, !player);

				if (maxCifra < c) {
					maxCifra = c;
					if (depth == startDepth)
						direction = 0;
				}
				tempGrid = newGrid.copy();
			}

			if (tempGrid.moveDown()) {
				var c = expectiMax(depth - 1, tempGrid, !player);

				if (maxCifra < c) {
					maxCifra = c;
					if (depth == startDepth)
						direction = 1;
				}
				tempGrid = newGrid.copy();
			}
			if (tempGrid.moveLeft()) {
				var c = expectiMax(depth - 1, tempGrid, !player);

				if (maxCifra < c) {
					maxCifra = c;
					if (depth == startDepth)
						direction = 2;
				}
				tempGrid = newGrid.copy();
			}
			if (tempGrid.moveRight()) {
				var c = expectiMax(depth - 1, tempGrid, !player);

				if (maxCifra < c) {
					maxCifra = c;
					if (depth == startDepth)
						direction = 3;
				}
			}
			if (maxCifra == Number.NEGATIVE_INFINITY) {
				return -100000000;
			}
			return maxCifra;
		}

		var emptySpots = newGrid.getEmptySpots();
		var tempGrid = newGrid.copy();
		var avgScore = 0;

		for (var i = 0; i < emptySpots.length; i++) {
			var s = emptySpots[i];
			tempGrid.grid[s.i][s.j] = new Tile(2);
			avgScore += 0.9 * expectiMax(depth - 1, tempGrid, !player);

			tempGrid.grid[s.i][s.j] = new Tile(4);
			avgScore += 0.1 * expectiMax(depth - 1, tempGrid, !player);

			tempGrid.grid[s.i][s.j] = null;
		}

		return avgScore / (2 * emptySpots.length);
	}

	function weightHeuristic(grid) {
		var sum = 0;
		var penalty = 0;
		for (var i = 0; i < 4; i++) {
			for (var j = 0; j < 4; j++) {
				if (grid.grid[i][j] !== null) {
					sum += weightMatrix[i][j] * grid.grid[i][j].value;
					//penalty += neighbourPenalty(i, j, grid, grid.grid[i][j].value);
				}
			}
		}
		return sum - penalty;
	}
	startAI();
}
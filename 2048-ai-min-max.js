function startMiniMaxAI() {
	var depth = 6;
	var weightMatrix = [
		[8, 5, 4, 3],
		[5, 4, 3, 2],
		[4, 3, 2, 1],
		[3, 2, 1, 0]
	];

	function minMaxAI() {
		var computeNextMove = function() {
			var dir = minMax(depth, grid, true);
			switch (dir.direction) {
				case "up":
					grid.moveUp();
					break;
				case "down":
					grid.moveDown();
					break;
				case "left":
					grid.moveLeft();
					break;
				case "right":
					grid.moveRight();
					break;
			}
			grid.isGameOver();
			if (!grid.gameOver) {
				grid.spawnRandomNumber();
				draw();
				setTimeout(computeNextMove, 1);
			} else {
				alert("GameOver");
			}
		}
		computeNextMove();
	}

	function minMax(depth, newGrid, player) {
		if (depth == 0) {
			newGrid.isGameOver();
			if (newGrid.gameOver) {
				return {
					score: -10000000
				};
			}
			return {
				score: (weightHeuristic(newGrid) - penaltyHeuristic(newGrid))
			};
		}

		var boardScores = [];

		if (player) {
			var tempGrid = newGrid.copy();
			if (tempGrid.moveUp()) {
				boardScores.push({
					score: minMax(depth - 1, tempGrid, !player),
					direction: "up"
				});
				tempGrid = newGrid.copy();
			}
			if (tempGrid.moveDown()) {
				boardScores.push({
					score: minMax(depth - 1, tempGrid, !player),
					direction: "down"
				});
				tempGrid = newGrid.copy();
			}
			if (tempGrid.moveLeft()) {
				boardScores.push({
					score: minMax(depth - 1, tempGrid, !player),
					direction: "left"
				});
				tempGrid = newGrid.copy();
			}
			if (tempGrid.moveRight()) {
				boardScores.push({
					score: minMax(depth - 1, tempGrid, !player),
					direction: "right"
				});
			}
			return findMax(boardScores);
		}

		var emptySpots = newGrid.getEmptySpots();
		var tempGrid = newGrid.copy();

		for (var i = 0; i < emptySpots.length; i++) {
			var s = emptySpots[i];
			tempGrid.grid[s.i][s.j] = new Tile(2);
			boardScores.push(0.9 * minMax(depth - 1, tempGrid, !player).score);

			tempGrid.grid[s.i][s.j] = new Tile(4);
			boardScores.push(0.1 * minMax(depth - 1, tempGrid, !player).score);

			tempGrid.grid[s.i][s.j] = null;
		}

		return findMin(boardScores);
	}

	function findMax(array) {
		if (array.length == 0) {
			return {
				score: -10000000,
				direction: "GameOver"
			};
		}

		var max = array[0];
		for (var i = 1; i < array.length; i++) {
			if (max.score < array[i].score) {
				max = array[i];
			}
		}
		return max;
	}

	function findMin(array) {
		if (array.length == 0) {
			return {
				score: 10000000,
				direction: "GameOver"
			};
		}

		var min = array[0];
		for (var i = 1; i < array.length; i++) {
			if (min > array[i]) {
				min = array[i];
			}
		}
		return min;
	}

	function weightHeuristic(grid) {
		var sum = 0;
		for (var i = 0; i < 4; i++) {
			for (var j = 0; j < 4; j++) {
				if (grid.grid[i][j] !== null) {
					sum += weightMatrix[i][j] * grid.grid[i][j].value;
				}
			}
		}
		return sum;
	}

	function penaltyHeuristic(grid) {
		var penalty = 0;
		for (var i = 0; i < 4; i++) {
			for (var j = 0; j < 4; j++) {
				if (grid.grid[i][j] !== null) {
					penalty += neighbourPenalty(i, j, grid, grid.grid[i][j].value);
				}
			}
		}
		return penalty;
	}

	function neighbourPenalty(i, j, grid, value) {
		var penalty = 0;
		if (j + 1 <= 3) {
			if (grid.grid[i][j + 1] !== null) {
				penalty += Math.abs(value - grid.grid[i][j + 1].value);
			}
		}
		if (i + 1 <= 3) {
			if (grid.grid[i + 1][j] !== null) {
				penalty += Math.abs(value - grid.grid[i + 1][j].value);
			}
		}

		if (j - 1 >= 0) {
			if (grid.grid[i][j - 1] !== null) {
				penalty += Math.abs(value - grid.grid[i][j - 1].value);
			}
		}
		if (i - 1 >= 0) {
			if (grid.grid[i - 1][j] !== null) {
				penalty += Math.abs(value - grid.grid[i - 1][j].value);
			}
		}
		return penalty;
	}
	minMaxAI();
}